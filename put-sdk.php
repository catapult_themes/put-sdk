<?php
/*
Plugin Name: PUT SDK
Plugin URI: https://catapultthemes.com/
Description: Just a testing plugin for the Plugin Usage Tracker
Version: 1.2.2
Author: Catapult Themes
Author URI: https://catapultthemes.com/
Text Domain: put-sdk
Domain Path: /languages
*/

// Exit if accessed directly

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Stop editing here
if( ! class_exists( 'Plugin_Usage_Tracker') ) {
	require_once dirname( __FILE__ ) . '/tracking/class-plugin-usage-tracker.php';
}
require_once dirname( __FILE__ ) . '/tracking/class-example-settings.php';

/**
 * If you are copying and pasting this code to another plugin you must rename the function below
 * The function must have a unique name so that it won't conflict with any other plugins using this tracker
 */
if( ! function_exists( 'your_plugin_prefix_start_plugin_tracking' ) ) { 	// Replace function name here
	function your_plugin_prefix_start_plugin_tracking() { 					// Replace function name
		$PUT = new Plugin_Usage_Tracker(
			__FILE__,
			'https://wisdomplugin.com/',				// Replace with the URL to the site where you will track your plugins
			array('wisdom_example_options_settings'),						// You can specify options here
			true,															// End-user opt-in is required by default
			true,															// Include deactivation form
			1																// Marketing
		);
	}
	your_plugin_prefix_start_plugin_tracking();
}

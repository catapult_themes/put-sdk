=== PUT SDK ===
Contributors: Catapult_Themes
Donate Link: https://catapultthemes.com/
Tags: analytics testing plugin
Requires at least: 4.3
Tested up to: 4.9.2
Stable tag: 1.2.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Just used for testing

== Description ==

N/A

== Installation ==

N/A

== Frequently Asked Questions ==

N/A

== Screenshots ==

N/A

== Changelog ==

= 1.2.2, 22 January 2018 =
* Added: do_tracking(true) to schedule_tracking
* Added: unset wisdom_last_track_time on deactivation 

= 1.2.1, 17 January 2018 =
* Added: force_tracking function

= 1.2.0, 11 January 2018 =
* Added: theme support

= 1.1.2, 28 December 2017 =
* Added: get and set email address
* Added: force parameter to do_tracking
* Updated: save current user's email address, not admin email

= 1.1.1, 1 August 2017 =
* Added: set last tracked time
* Added: check last tracked time
* Updated: cron to daily

= 1.1.0, 29 July 2017 =
* Added: option to opt out of tracking

= 1.0.3, 2 May 2017 =
* Fixed: Print deactivation buttons text

= 1.0.2, 8 April 2017 =
* Updated: Wrapped is_rtl in function_exists

= 1.0.1, 23 March 2017 =
* Added: translatable strings on deactivation form buttons

= 1.0.0 =
* Initial commit

== Upgrade Notice ==

N/A
